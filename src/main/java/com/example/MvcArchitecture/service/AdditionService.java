package com.example.MvcArchitecture.service;

import com.example.MvcArchitecture.model.Addition;
import org.springframework.stereotype.Service;

@Service
public class AdditionService {
    public int addition(Addition additionObject){
        int result=additionObject.getFirstNumber()+additionObject.getSecondNumber();
        additionObject.setResult(result);
        return  result;
    }
}
