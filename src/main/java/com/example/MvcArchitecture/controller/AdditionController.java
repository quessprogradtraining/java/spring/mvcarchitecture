package com.example.MvcArchitecture.controller;

import com.example.MvcArchitecture.model.Addition;
import com.example.MvcArchitecture.service.AdditionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdditionController {
    @Autowired
    AdditionService additionServiceObject;

    @GetMapping("/addition")
    public String addition(){
        Addition additionObj=new Addition(2,3);
        int result=additionServiceObject.addition(additionObj);
        return "addition is:"+result;
    }
}
