package com.example.MvcArchitecture;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class AdditionAspect {
    @Before("execution(* com.example.MvcArchitecture.controller.AdditionController.addition())")
    public void beforeAddition(){
        System.out.println("addition process started");
    }
    @After("execution(* com.example.MvcArchitecture.controller.AdditionController.addition())")
    public void afterAddition(){
        System.out.println("addition process completed");
    }

}
